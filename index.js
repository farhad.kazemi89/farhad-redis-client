const Redis = require("redis");
const c = require("./config");
// require('dotenv').config({path: './.env'});

let host = c.host;
let port = c.port;
const redisClient = Redis.createClient({
  host,
  port,
});

let redisConnection;
let cReconnect = 0;

redisClient.on("connect", () => {
  console.log(`Client connected to redis on server=${host}:${port}`);
});

redisClient.on("ready", () => {
  redisConnection = true;
  console.log(`Client is ready to use`);
});

redisClient.on("error", (err) =>{
  redisConnection = false;
  console.log("Redis client error", err.message)
});


redisClient.on("reconnecting", () =>{
  cReconnect++;
  console.log(`Redis client reconnecting number ${cReconnect}`);
});

redisClient.on("end", () => console.log("Client disconnected from redis!"));

(async () => {
  try {
    await redisClient.connect((err) => {
      throw new Error(`Redis connection error: ${err}`);
    });    
  } catch (err) {
    console.log(`db error connect: ${err}`);
  }
})();

// process.on("SIGINT", () => {
//   redisClient.quit();
// });

module.exports = {
  redisClient, 
  redisConnection
};