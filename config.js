const db = {
  user: process.env.DB_USERNAME || "redis",
  host: process.env.DB_HOST || "localhost",
  database: process.env.DB_DATABASE || 0,
  password: process.env.DB_PASSWORD || "",
  port: process.env.DB_PORT || 6379,
};

module.exports = db;